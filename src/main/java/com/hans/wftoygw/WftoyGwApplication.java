package com.hans.wftoygw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WftoyGwApplication {

    public static void main(String[] args) {
        SpringApplication.run(WftoyGwApplication.class, args);
    }

}
